package com.compra.demo.Domain;

public class Result {

    private Integer status;
    private String errorMessage;
    private Double amount;

    public Result() {

    }

    public Result(Integer status, String errorMessage, Double amount) {
        this.status = status;
        this.errorMessage = errorMessage;
        this.amount = amount;
    }

    public Double getAmount() {
        return amount;
    }
}
