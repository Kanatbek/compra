package com.compra.demo.Service;

import com.compra.demo.Domain.Currency;

public interface CurrencyService {
    Currency getRate(String currencyFrom, String currencyTo, Double summ) throws Exception;
}
