package com.compra.demo.ServiceImpl;

import com.compra.demo.Domain.Currency;
import com.compra.demo.Domain.Result;
import com.compra.demo.Service.CurrencyService;
import jdk.nashorn.internal.parser.JSONParser;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParser;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;

@Service
public class CurrencyServiceImpl implements CurrencyService {
    @Override
    public Currency getRate(String currencyFrom, String currencyTo, Double summ) throws Exception {

        Result res = getRates(currencyFrom, currencyTo, summ);

        Currency currency = new Currency(currencyFrom, currencyTo, res.getAmount());
        return currency;
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    private Result getRates(String currencyFrom, String currencyTo, Double summ) {

        String url_str = "https://www.amdoren.com/api/currency.php?api_key=L9Pvyt7XCPnqaAhDR5sXMfu43VfHEC&from=" +
                currencyFrom +
                "&to=" + currencyTo +
                "&amount=" + summ;

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>(headers);

        return restTemplate().exchange(url_str, HttpMethod.GET, entity, Result.class).getBody();
    }
}
