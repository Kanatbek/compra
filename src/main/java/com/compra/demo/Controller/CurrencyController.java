package com.compra.demo.Controller;

import com.compra.demo.Domain.Currency;
import com.compra.demo.Service.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@RepositoryRestController
@RequestMapping(value = "currency")
public class CurrencyController {

    @Autowired
    CurrencyService currencyService;

    @RequestMapping(method = RequestMethod.GET, value = "/getRate")
    @ResponseBody
    public Currency getRate(
            @RequestParam(value = "from", required = true) String currencyFrom,
            @RequestParam(value = "to", required = true) String currencyTo,
            @RequestParam(value = "summ", required = true) Double summ
    ) {
        try {
            Currency currency = currencyService.getRate(currencyFrom, currencyTo, summ);
            return currency;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }


    }


}
